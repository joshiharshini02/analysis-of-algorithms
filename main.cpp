#include <iostream>

using namespace std;

void print_array(char* heading, int arr[], int size)
{
    cout << heading << endl;
    for (int i=0;i<size;++i)
    {
        cout << arr[i] << "\t";        
    }
    cout << endl;
}

int main()
{

    int size = rand()%10;
    int arr[size];

    for (int i=0;i<size;++i)
    {
        arr[i]=rand()%100;        
    }

    print_array((char*)"Before sorting",arr,size);
    cout << endl;
    
    int comparision = 0;
    
    for (int i = 0; i < (size - 1); ++i)
	{
		cout << "iteration number : " << i+1 << endl;
		for (int j = 0; j < size - 1 - i; j++)
	    {
			// instead of a, it is arr[i] and instead of b it is arr[i+1]
			cout << "Comparing arr[" << j << "] with arr[" << j+1 << "], that is comparing " << arr[j] << " with " << arr[j + 1] << endl;
			++comparision;
			if (arr[j] > arr[j + 1])
			{
				cout << "Swap required" << endl;
				int temp = arr[j + 1];
				arr[j + 1] = arr[j];
				arr[j] = temp;
			}
			else
			{
			    cout << "Swap NOT required" << endl;
			}
		}
	    print_array((char*)"End of iteration",arr,size);
        cout << endl;
        
	}
	
    print_array((char*)"After sorting",arr,size);
    
    cout << "Total comparision : " << comparision << endl;

    

    return 0;
}

